#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "bd.h"
#include <string.h>
#include <omp.h>

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p, cse6230nrand_t* RN)
{
  double fconst = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;
  double old_positions[3*npos];

  for (int step=0; step<INTERVAL_LEN; step++)
  {
    int retval;
    int numpairs = 0;

    while (1) {
      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
      if (!retval) break;
      if (retval == -1) {
        free(pairs);
        free(dist2);
        maxnumpairs *= 2;
        dist2 = (double *) malloc(maxnumpairs*sizeof(double));
        pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
        assert(dist2);
      } else {
        return retval;
      }
    }

    ////////////////////////////  Compute Forces  //////////////////////////////////////////////
    //
  //const double krepul = 100.;
  int p;
  memcpy(old_positions, pos, npos*3*sizeof(double));

  #pragma omp parallel //num_threads(4) 
  {
  double dx, dy, dz, s2, s, f, krepul=100;
  static double a = 0.2; /* particle radius for this collision problem */
  #pragma for schedule(guided)
  for (int p = 0; p < numpairs; p++) {
        double s = sqrt(dist2[p]);
        double forces = krepul*(2 - s); 
        int i = pairs[2*p];
        int j = pairs[2*p+1];
        const double* ri = &old_positions[3*i];
        const double* rj = &old_positions[3*j];
        dx = remainder(ri[0]-rj[0], L);
        dy = remainder(ri[1]-rj[1], L);
        dz = remainder(ri[2]-rj[2], L);
        #pragma atomic
        pos[3*i+0] += DELTAT*forces*dx/s;
        #pragma atomic
        pos[3*i+1] += DELTAT*forces*dy/s;
        #pragma atomic
        pos[3*i+2] += DELTAT*forces*dz/s;
        #pragma atomic
        pos[3*j+0] -= DELTAT*forces*dx/s;
        #pragma atomic
        pos[3*j+1] -= DELTAT*forces*dy/s;
        #pragma atomic
        pos[3*j+2] -= DELTAT*forces*dz/s;

  }
  /*
  for (int p = 0; p < numpairs; p++) {

            double ri[] = {pos[3*i], pos[3*i+1], pos[3*i+2]};
            double rj[] = {pos[3*j], pos[3*j+1], pos[3*j+2]};

            //printf("%g \n", pos[3*i]);
            dx = remainder(ri[0]-rj[0], L);
            dy = remainder(ri[1]-rj[1], L);
            dz = remainder(ri[2]-rj[2], L);
        s2 = dx*dx + dy*dy + dz*dz; 

        if (s2 < 4.*a*a) {
          s = sqrt(s2);
          f = krepul*(2.*a-s);
          #pragma atomic
          forces[3*i+0] += f*dx/s;
          #pragma atomic
          forces[3*i+1] += f*dy/s;
          #pragma atomic
          forces[3*i+2] += f*dz/s;
          #pragma atomic
          forces[3*j+0] -= f*dx/s;
          #pragma atomic
          forces[3*j+1] -= f*dy/s;
          #pragma atomic
          forces[3*j+2] -= f*dz/s;
        
        }
    }*/
  }
      /* TODO: Put the force calculation here using the same formula as exercise 04*/
      /* TODO: Take out this print statement */
#if 0
      if (!step) {
        printf("Particle pair %d: (%d, %d) are %g apart\n",p,pairs[2*p],pairs[2*p+1],sqrt(dist2[p]));
      }
#endif
    

    // update positions with Brownian displacements
    // for (int i=0; i<3*npos; i++)
    // {
    //   double noise = cse6230nrand(nrand);

    //   pos[i] += f*noise;
    // }
    int j;
    #pragma omp parallel //num_threads(4)
    {
    
    
    //unsigned int seed = omp_get_thread_num();

    ///////////////////////////////////////////////  RandR Parallelization      ///////////////////////////////////////////////////
    // a randomly generated sequence of seeds was created in harness main at the address of RN to ensure that each thread a randomly generated sequence
    #pragma omp for 
    for (j = 0; j < 3 * npos; j++) {
      int thread_num = omp_get_thread_num();
      double noise = cse6230nrand(&RN[thread_num]);
      #pragma omp atomic
      pos[j] += fconst*noise;

      //double noise = (int) cse6230nrand(&seed)/ (double) RAND_MAX * 2 -1;

      //pos[j] += remainder((M * forces[j] + sqrt(2. * dt) * noise)+.5, 1)+.5;
      //pos[j] += abs(remainder(M * forces[j] + sqrt(2. * dt) * noise, 1));
    }
    }




  }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}
