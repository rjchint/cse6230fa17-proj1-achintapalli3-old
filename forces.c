#include "forces.h"
#include <math.h>
#include <stdio.h>

//two for blocks such that operations dont write at same time
//use omp_bind_proc


void compute_forces(int N, const double *pos, double *forces)

/*{
  int i;

  for (i = 0; i < 3 * N; i++) {
    forces[i] = 0.;
  }
}
*/
{
	int i;

	#pragma omp parallel num_threads(4) 
	{
	double dx, dy, dz, s2, s, f, krepul=100, L=1;
	static double a = 0.2; /* particle radius for this collision problem */
	#pragma for schedule(guided)
	for (i=0; i<N; i++) {
		for (int j=i+1; j<N; j++) {

		        double ri[] = {pos[3*i], pos[3*i+1], pos[3*i+2]};
		        double rj[] = {pos[3*j], pos[3*j+1], pos[3*j+2]};

		        //printf("%g \n", pos[3*i]);
		        dx = remainder(ri[0]-rj[0], L);
		        dy = remainder(ri[1]-rj[1], L);
		        dz = remainder(ri[2]-rj[2], L);
				s2 = dx*dx + dy*dy + dz*dz; 

				if (s2 < 4.*a*a) {
					s = sqrt(s2);
					f = krepul*(2.*a-s);
					forces[3*i+0] += f*dx/s;
					forces[3*i+1] += f*dy/s;
					forces[3*i+2] += f*dz/s;
				
				}
		}
	}
	}
		#pragma omp parallel num_threads(4) 
	{
	double dx, dy, dz, s2, s, f, krepul=100, L=1;
	static double a = 0.2; /* particle radius for this collision problem */
	#pragma for schedule(guided)
	for (i=0; i<N; i++) {
		for (int j=0; j<i; j++) {
		        double ri[] = {pos[3*i], pos[3*i+1], pos[3*i+2]};
		        double rj[] = {pos[3*j], pos[3*j+1], pos[3*j+2]};

		        //printf("%g \n", pos[3*i]);
		        dx = remainder(ri[0]-rj[0], L);
		        dy = remainder(ri[1]-rj[1], L);
		        dz = remainder(ri[2]-rj[2], L);
				s2 = dx*dx + dy*dy + dz*dz; 

				if (s2 < 4.*a*a) {
					s = sqrt(s2);
					f = krepul*(2.*a-s);
					forces[3*j+0] -= f*dx/s;
					forces[3*j+1] -= f*dy/s;
					forces[3*j+2] -= f*dz/s;
				
				}
		}
	}
	}
}
/*
					#pragma atomic
					forces[3*i+0] += f*dx/s;
					#pragma atomic
					forces[3*i+1] += f*dy/s;
					#pragma atomic
					forces[3*i+2] += f*dz/s;
					#pragma atomic
					forces[3*j+0] -= f*dx/s;
					#pragma atomic
					forces[3*j+1] -= f*dy/s;
					#pragma atomic
					forces[3*j+2] -= f*dz/s;

					*/
